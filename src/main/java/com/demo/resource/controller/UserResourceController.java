package com.demo.resource.controller;

import com.demo.resource.dto.UserDetailsDTO;
import com.demo.resource.feignService.UserServiceFeign;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
@RestController
@RequestMapping("/api")
public class UserResourceController {

    private final Logger LOGGER = LoggerFactory.getLogger(UserResourceController.class);

    @Autowired
        private UserServiceFeign userServiceFeign;

        @PostMapping(value = "/saveUpdate")
        public UserDetailsDTO saveUpdate(@RequestBody UserDetailsDTO userDetailsDTO){
            LOGGER.info("Entering into resource service to save user details::" + new Timestamp(new Date().getTime()));
            return userServiceFeign.saveUpdate(userDetailsDTO);
        }

        @GetMapping(value = "/getById/{id}")
        public UserDetailsDTO getUserById(@PathVariable Long id){
            return userServiceFeign.getUserById(id);
        }

        @GetMapping(value = "/getByName/{name}")
        public List<UserDetailsDTO> getUserByName(@PathVariable String name){
            return userServiceFeign.getUserByName(name);
        }

    }
